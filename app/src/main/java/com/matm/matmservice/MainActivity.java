package com.matm.matmservice;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.finopaytech.finosdk.activity.DeviceSettingActivity;
import com.finopaytech.finosdk.activity.MainTransactionActivity;
import com.finopaytech.finosdk.encryption.AES_BC;
import com.finopaytech.finosdk.helpers.FinoApplication;
import com.finopaytech.finosdk.helpers.Utils;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    String RequestData="";
    String HeaderData = "";
    String ReturnTime = "";
    String flagBluetooth ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FinoApplication.init(this);
        CallFinoApp();
    }

    private void CallFinoApp() {

        Bundle b = getIntent().getExtras();
        if(b!=null){
            RequestData = b.getString("RequestData");
            HeaderData = b.getString("HeaderData");
            ReturnTime = b.getString("ReturnTime");
            flagBluetooth = b.getString("Flag");

            if(flagBluetooth.equalsIgnoreCase("bluetooth")){
                Intent settingIntent = new Intent(MainActivity.this, DeviceSettingActivity.class);
                settingIntent.putExtra("IS_PAIR_DEVICE",true);
                startActivityForResult(settingIntent,3);
                finish();
            } if(flagBluetooth.equalsIgnoreCase("update")){
                Intent settingIntent = new Intent(MainActivity.this, DeviceSettingActivity.class);
                settingIntent.putExtra("IS_PAIR_DEVICE",true);
                startActivityForResult(settingIntent,3);
                finish();
            }
            else{
                Intent intent = new Intent(getBaseContext(), MainTransactionActivity.class);
                intent.putExtra("RequestData", RequestData);
                intent.putExtra("HeaderData", HeaderData);
                intent.putExtra("ReturnTime", ReturnTime);// Application return time in second
                startActivityForResult(intent, 1);
                finish();
            }


           // finish();

            // and any other data that the other app sent
            //Toast.makeText(MainActivity.this,myString,Toast.LENGTH_SHORT).show();




        }else{
            Toast.makeText(MainActivity.this,"Not found.",Toast.LENGTH_SHORT).show();
        }



  /*      authentication = microAtmResponse.getAuthentication();
        encData = microAtmResponse.getEncData();
        Intent intent = new Intent(getBaseContext(), MainTransactionActivity.class);
        intent.putExtra("RequestData", encData);
        intent.putExtra("HeaderData", authentication);
        intent.putExtra("ReturnTime", 5);// Application return time in second
        startActivityForResult(intent, 1);*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null & resultCode == RESULT_OK) {
             if (requestCode == 1) {
                String response;
                if (data.hasExtra("ClientResponse")) {
                    response = data.getStringExtra("ClientResponse");

                    //Bundle bundle = new Bundle();
                    //bundle.putString("value",data);
                    Intent mIntent = new Intent();
                   // mIntent.putExtras(bundle);
                    startActivityForResult(data, 1);
                    super.onBackPressed();

                }
        }else if(requestCode==3){

                 String response;
                 if (data.hasExtra("DeviceConnectionDtls")) {
                     response = data.getStringExtra("DeviceConnectionDtls");
                     String[] error_dtls = response.split("\\|");
                     String errorMsg = error_dtls[0];
                     String errorMsg2 = error_dtls[1];
                     Intent mIntent = new Intent();
                     // mIntent.putExtras(bundle);
                     startActivityForResult(data, 3);
                     super.onBackPressed();

                 }

             }
        }
    }
}
